package vyhidna.lambdas.fourthTask;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FourthTask {
  Scanner scanner = new Scanner(System.in);

  public List<String> readValuesFromUser(){
    List<String> valuesFromUser = new ArrayList<>();
    String inputtedLine = scanner.nextLine();
    while (!inputtedLine.equals("")){
      valuesFromUser.add(inputtedLine);
      inputtedLine= scanner.nextLine();
    }
    return valuesFromUser;
  }

  public Integer numberOfUniqueWords(List<String> strings){
    return (int)strings
        .stream()
        .flatMap(s -> Stream.of(s.split(" ")))
        .distinct()
        .count();
  }

  public List<String> sortedListOfUniqueValues(List<String> strings){
   return strings
        .stream()
        .flatMap(s -> Stream.of(s.split(" ")))
        .distinct()
        .sorted(Comparator.naturalOrder())
        .collect(Collectors.toList());
  }

  public Map<String, Long> numberOfAllWords(List<String> strings){
    return strings.stream()
        .flatMap(s -> Stream.of(s.split(" ")))
        .collect(groupingBy(Function.identity(), counting()));
  }

  public Map<String, Long> numberOfOllSymbolsExceptUpperCase(List<String> strings){
    return strings.stream()
        .flatMap(s->Stream.of(s.split("")))
        .filter(l->(l.getBytes()[0]<64||l.getBytes()[0]>90))
        .collect(groupingBy(Function.identity(), counting()));
  }

}
