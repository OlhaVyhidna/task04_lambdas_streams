package vyhidna.lambdas.fourthTask;

import java.util.List;
import java.util.Map;

public class Main {

  public static void main(String[] args) {
    FourthTask fourthTask = new FourthTask();
     List<String> list = fourthTask.readValuesFromUser();
    System.out.println(list);
    Map<String, Long> stringLongMap = fourthTask.numberOfAllWords(list);
    System.out.println(stringLongMap);
    Map<String, Long> stringLongMap1 = fourthTask.numberOfOllSymbolsExceptUpperCase(list);
    System.out.println(stringLongMap1);
    Integer integer = fourthTask.numberOfUniqueWords(list);
    System.out.println(integer);
    List<String> list1 = fourthTask.sortedListOfUniqueValues(list);
    System.out.println(list1);
  }

}
