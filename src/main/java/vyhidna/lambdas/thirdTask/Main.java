package vyhidna.lambdas.thirdTask;

import java.util.IntSummaryStatistics;
import java.util.List;

public class Main {

  public static void main(String[] args) {
    ThirdTask thirdTask = new ThirdTask();
    List<Integer> integers = thirdTask.secondListGenerator();
    List<Integer> integers1 = thirdTask.thirdListGenerator();
    thirdTask.averageValue(integers);
    System.out.println(thirdTask.summaryStatistics(integers));
    IntSummaryStatistics intSummaryStatistics = thirdTask.summaryStatistics(integers1);
    System.out.println(intSummaryStatistics);
    thirdTask.max(integers1);
    thirdTask.max(integers);
    thirdTask.min(integers);
    Integer integer = thirdTask.sumUsingReduce(integers1);
    System.out.println(integer);
  }

}
