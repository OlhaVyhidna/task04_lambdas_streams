package vyhidna.lambdas.thirdTask;

import java.util.Comparator;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ThirdTask {

  public List<Integer> firstListGenerator(int listSize){
    return Stream.generate(new Random()::nextInt).limit(listSize).collect(Collectors.toList());
  }

  public List<Integer> secondListGenerator(){
    return IntStream.iterate(0, i -> i + 2)
        .mapToObj(Integer::valueOf)
        .limit(10)
        .collect(Collectors.toList());
  }

  public List<Integer> thirdListGenerator(){
   return Stream.generate(() -> (new Random()).nextInt(100))
        .limit(10)
        .collect(Collectors.toList());
  }

  public void averageValue(List<Integer> integers){
   integers.stream()
       .mapToInt(Integer::intValue)
       .average()
       .ifPresent(value -> System.out
        .println("Average found is " + value));
  }

  public IntSummaryStatistics summaryStatistics(List<Integer> integers){
return integers.stream()
    .mapToInt((x) -> x)
    .summaryStatistics();
  }

  public void min(List<Integer> integers){
    integers.stream()
        .min(Comparator.comparing(integer -> integer))
        .ifPresent(min -> System.out.println("Minimum found is " + min));
  }
  public void max(List<Integer> integers){
    integers.stream()
        .max(Comparator.comparing(integer -> integer))
        .ifPresent(max -> System.out.println("Maximum found is " + max));
  }

  public Integer sumUsingStream(List<Integer> integers){
   return integers.stream().mapToInt(x->x).sum();
  }

  public Integer sumUsingReduce(List<Integer> integers){
    return integers.stream().reduce(0, (subtotal, element) -> subtotal + element);
  }

  public Integer numberOfValueBigerThanAverage(List<Integer> integers, int average){
    return (int)integers.stream().filter(x -> x>average).count();
  }

}
