package vyhidna.lambdas.secondtask;

public class MyReceiver {

public void toUpperCase(String argument){
  System.out.println(argument.toUpperCase());
}

public void getSecondHalfOfArgumentMessage(String argument){
  System.out.println(argument.substring(argument.length()>>1));
}

public void splitArgument(String argument){
   String[] strings = argument.split("");
  for (String string : strings) {
    System.out.print(string + " ");
  }
  System.out.println();
}

public void getArgumentInBytes(String argument){
  byte[] argumentBytes = argument.getBytes();
  for (byte argumentByte : argumentBytes) {
    System.out.print(argumentByte + " ");
  }
  System.out.println();
}

}
