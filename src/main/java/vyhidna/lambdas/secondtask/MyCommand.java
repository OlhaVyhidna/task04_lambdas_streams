package vyhidna.lambdas.secondtask;

public class MyCommand {
  private MyReceiver receiver = new MyReceiver();

  MyInterface toUpperCase = s -> receiver.toUpperCase(s);

  MyInterface getArgumentInBytes = receiver::getArgumentInBytes;

  MyInterface getSecondHalfOfArgumentMessage = new MyInterface() {
    @Override
    public void argumentAction(String argument) {
      receiver.getSecondHalfOfArgumentMessage(argument);
    }
  };

  MyInterface splitArgument = new MyInterfaceSplitArgumentImpl();

  private class MyInterfaceSplitArgumentImpl implements MyInterface{

    @Override
    public void argumentAction(String argument) {
      receiver.splitArgument(argument);
    }
  }
}
