package vyhidna.lambdas.secondtask;

import java.util.Scanner;

public class Client {
 private Scanner scanner = new Scanner(System.in);
 private MyCommand myCommand = new MyCommand();
 private Invoker invoker = new Invoker();
 private final String CHOOSE_METHOD = "Choose method to invoke:";
 private final String METHOD_1 = "1 - toUpperCase, implemented as lambda function";
 private final String METHOD_2 = "2 - getArgumentInBytes, implemented as method reference";
 private final String METHOD_3 = "3 - getSecondHalfOfArgumentMessage, implemented as anonymous class";
 private final String METHOD_4 = "4 - splitArgument, implemented as as object of command class";


  public void startProcess(){
    int methodNumber = getMethodNumber();
    MyInterface invokeMethod = invokeMethod(methodNumber);
    String argument = readArgument();
    invoker.executeOperation(invokeMethod, argument);
  }

  private MyInterface invokeMethod(int methodNumber){
    switch (methodNumber){
      case 1:{
        return myCommand.toUpperCase;
      }
      case 2:{
        return myCommand.getArgumentInBytes;
      }
      case 3:{
        return myCommand.getSecondHalfOfArgumentMessage;
      }
      default:{
        return myCommand.splitArgument;
      }
    }
  }

  private int getMethodNumber(){
    System.out.println(CHOOSE_METHOD);
    System.out.println(METHOD_1);
    System.out.println(METHOD_2);
    System.out.println(METHOD_3);
    System.out.println(METHOD_4);
    String userInput = scanner.nextLine();
    int methodNumber = -1;
    try {
      methodNumber = Integer.parseInt(userInput);
    }catch (NumberFormatException e){
      System.out.println("Please, enter e number");
      getMethodNumber();
    }
    if (methodNumber<1||methodNumber>4){
      System.out.println("Please, enter number from proposed");
      getMethodNumber();
    }
    return methodNumber;
  }

  private String readArgument(){
    System.out.println("Please, enter an argument");
    return scanner.nextLine();
  }
}
