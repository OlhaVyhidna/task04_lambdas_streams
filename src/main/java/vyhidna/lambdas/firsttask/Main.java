package vyhidna.lambdas.firsttask;

public class Main {

  public static void main(String[] args) {
     FirstTask firstTask = new FirstTask();

    System.out.println(firstTask.maxValue.method(7, 12, 9));
    System.out.println(firstTask.average.method(4, 8, 5));
  }

}
