package vyhidna.lambdas.firsttask;

public class FirstTask {

    FunctionalInterface maxValue = ((first, second, third) ->
    {
      int bigger = maxOfTwo(first, second);
      return maxOfTwo(bigger, third);
    });

    FunctionalInterface average = ((first, second, third) -> ((first+second+third)/3));

  private static int maxOfTwo(int a, int b){
    return a>b?a:b;
  }

  interface FunctionalInterface{
int method(int first, int second, int third);
  }

}
